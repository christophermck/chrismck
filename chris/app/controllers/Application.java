package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.Item;
import models.Staff;
import play.db.DB;
import play.mvc.Controller;
import play.mvc.Result;
import scala.collection.script.Remove;
import views.html.addform;
import views.html.allitems;
import views.html.allstaff;
import views.html.body;
import views.html.index;
import views.html.staff;

public class Application extends Controller {
	ArrayList<String> nameList;
	
    public static Result index() {
        return ok(index.render("Your new application is ready."));
        
        }
    
    public static Result forms() {

		return ok(body.render());
	}
    
    public static Result addform() {
    	
    	
		return ok(addform.render());
	}
    
    public static Result addstaff() {

		return ok(staff.render());
	}
    

    public static Result items() throws SQLException {
    	ArrayList<Item> itemsList = new ArrayList<Item>();
    
    	Connection c = DB.getConnection();	
    	String view = "Select work_item_id, staff_member_id, title, description, category, date_logged, completed From work_item";
    	PreparedStatement s = c.prepareStatement(view);
    	ResultSet rs = s.executeQuery(view);
    	
    	while (rs.next()){
    		Item newItem = new Item(rs.getString("work_item_id"),rs.getString("title"),rs.getString("description"), 
    				rs.getString("category"), rs.getString("date_logged"), rs.getString("completed"),
    				rs.getString("staff_member_id"));
    		itemsList.add(newItem);    
    	}
    	
		return ok(allitems.render(itemsList));
		
    }
    
    public static Result staff() throws SQLException {
    	ArrayList<Staff> staffList = new ArrayList<Staff>();
    
    	Connection c = DB.getConnection();	
    	String view = "Select staff_member_id, work_item_id, surname, email, business_unit, systems_team_member From staff_member";
    	PreparedStatement s = c.prepareStatement(view);
    	ResultSet rs = s.executeQuery(view);
    	
    	while (rs.next()){
    		Staff newStaff = new Staff(rs.getString("staff_member_id"),rs.getString("work_item_id"),
    				rs.getString("surname"), rs.getString("email"), rs.getString("business_unit"),
    				rs.getString("systems_team_member"));
    		staffList.add(newStaff);    
    	}
    	
		return ok(allstaff.render(staffList));
		
    }
    
//    public static Result delete() throws SQLException{
//    	Connection c = DB.getConnection();
//    	String view = "Delete from staff_member where staff_member_id=?";
//    	PreparedStatement s = c.prepareStatement(view);
//    	ResultSet rs = s.executeQuery(view);
//    	
//    	while (rs.next()){
//    		
//    	}
//		return ok(deletestaff.render());
//    	
//    }

}