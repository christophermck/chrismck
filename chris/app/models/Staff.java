package models;

public class Staff {
	private String staff_member_id;
	private String work_item_id;
	private String surname;
	private String email;
	private String business_unit;
	private String systems_team_member;
	
	
	public Staff(String new_staff_member_id, String new_work_item_id, String new_surname, 
			String new_email, String new_business_unit, String new_systems_team_member) {
		this.setStaff_member_id(new_staff_member_id);
		this.setWork_item_id(new_work_item_id);
		this.setSurname(new_surname);
		this.setEmail(new_email);
		this.setBusiness_unit(new_business_unit);
		this.setSystems_team_member(new_systems_team_member);
	}

	public String getStaff_member_id() {
		return staff_member_id;
	}

	private void setStaff_member_id(String staff_member_id) {
		this.staff_member_id = staff_member_id;
	}

	public String getWork_item_id() {
		return work_item_id;
	}

	private void setWork_item_id(String work_item_id) {
		this.work_item_id = work_item_id;
	}

	public String getSurname() {
		return surname;
	}

	private void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	private void setEmail(String email) {
		this.email = email;
	}

	public String getBusiness_unit() {
		return business_unit;
	}

	private void setBusiness_unit(String business_unit) {
		this.business_unit = business_unit;
	}

	public String getSystems_team_member() {
		return systems_team_member;
	}

	private void setSystems_team_member(String systems_team_member) {
		this.systems_team_member = systems_team_member;
	}

}
