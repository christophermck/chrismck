package models;

public class Item {
	private String work_item_id;
	private String title;
	private String description;
	private String category;
	private String date_logged;
	private String completed;
	private String staff_member_id;
	
	
	public Item(String new_work_item_id, String new_title, String new_description, String new_category, 
			String new_date_logged, String new_completed, String new_staff_member_id) {
		this.setWork_item_id(new_work_item_id);
		this.setTitle(new_title);
		this.setDescription(new_description);
		this.setCategory(new_category);
		this.setDate_logged(new_date_logged);
		this.setCompleted(new_completed);
		this.setStaff_member_id(new_staff_member_id);
	}

	public String getWork_item_id() {
		return work_item_id;
	}

	private void setWork_item_id(String work_item_id) {
		this.work_item_id = work_item_id;
	}

	public String getTitle() {
		return title;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	private void setCategory(String category) {
		this.category = category;
	}

	public String getDate_logged() {
		return date_logged;
	}

	private void setDate_logged(String date_logged) {
		this.date_logged = date_logged;
	}

	public String getCompleted() {
		return completed;
	}

	private void setCompleted(String completed) {
		this.completed = completed;
	}

	public String getStaff_member_id() {
		return staff_member_id;
	}

	private void setStaff_member_id(String staff_member_id) {
		this.staff_member_id = staff_member_id;
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}
	
}
